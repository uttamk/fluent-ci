require 'FileUtils'
require 'components/job_status'
module FluentCI
  module Components
    class Job
      def initialize name, block, log_dir = "."
        @name = name
        @block = block
        @log_dir = log_dir
        @status = JobStatus.new
      end
      attr_reader :log_dir
      attr_reader :build_number

      def run build_number=0

        puts "Running job #{@name}"
        log_path = "#{@log_dir}/#{build_number}"
        FileUtils.mkdir_p(log_path) unless File.exists?(log_path)
        @status  = @status.running
        File.write("#{log_path}/#{@name}.status", @status)
        old_stdout = $stdout
        old_stderr = $stderr

        begin
          File.open("#{log_path}/#{@name}.log", "w") do |file|
            $stdout = file
            $stderr = file
            begin
              @block.call
            rescue Exception => e
              $stderr.puts e.message
              $stderr.puts e.backtrace.inspect
              @status = @status.failure
              File.write("#{log_path}/#{@name}.status", @status)
            end
            @status = @status.success
            File.write("#{log_path}/#{@name}.status", @status)
          end
        ensure
          $stdout = old_stdout
          $stderr = old_stderr
        end
      end
    end
  end
end