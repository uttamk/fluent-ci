require 'fluentci/engine'

describe "Engine" do
   it "should send the correct log directory to the job" do

      stubbedRepo = double("repo")
      stubbedRepo.stub(:add_build_for).and_return(10)

      engine = FluentCI::Engine.new "./test_workspace", stubbedRepo
      engine.add_pipeline "test_pipeline"
      engine.add_stage "test_stage"
      engine.add_job "test_job", Proc.new {puts "Test job"}

      engine.run
      sleep 1
      expect(File.exist? "./test_workspace/test_pipeline/test_stage/10/test_job.log").to eq true
   end

   after(:each) do
      `rm -rf test_workspace`
   end
end
