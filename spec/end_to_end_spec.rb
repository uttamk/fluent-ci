require './lib/fluentci'

describe "Engine End to End" do
    before(:each) do
      `rm -rf workspace`
      `rm -rf "./My Code"`
    end

    it "should add a pipeline and run it" do

        content = File.read("config/test.rb")
        FluentCI::DSL.start
        FluentCI::DSL.eval content
        FluentCI::DSL.run
        #Waiting for the forked jobs to finish
        sleep(1)

        expect(File.exists? "./workspace/My Code/Compile And Unit Test/1/Compile.log").to eq(true)
        expect(File.exists? "./workspace//My Code/Compile And Unit Test/1/Unit Test.log").to eq(true)
        expect(File.exists? "./workspace//My Code/Javascript Tests/1/Run Coverage reports.log").to eq(true)

        expect(File.exists? "./workspace/My Code/Javascript Tests/1/Run Jasmine.log").to eq(true)
        (File.read "./workspace/My Code/Javascript Tests/1/Run Coverage reports.log").should include("Running Coverage reports\ncat: qwertyuiop.txt: No such file or directory\n")
    end

    after(:each) do
        `rm -rf "./My Code"`
        `rm -rf "workspace"`
    end

end