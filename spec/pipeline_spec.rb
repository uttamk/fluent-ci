require 'fluentci/components/pipeline'

describe "Pipeline" do
  it "should add stages and run them sequentially" do
    stage1 = double("stage1")
    stage2 = double("stage1")
    stage1.should_receive(:run)
    stage2.should_receive(:run)

    pipeline = FluentCI::Components::Pipeline.new("test_pipeline")
    pipeline.add_stage stage1
    pipeline.add_stage stage2
    pipeline.run

    expect(pipeline.stages.length).to eq(2)
  end
end
