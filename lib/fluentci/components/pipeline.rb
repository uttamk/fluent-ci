module FluentCI
  module Components
    class Pipeline
      attr_reader :name
      attr_reader :stages
      def initialize pipeline_name
        @name = pipeline_name
        @stages = []
      end

      def add_stage stage
        @stages << stage
      end

      def run build_number=0
        @stages.each {|stage| stage.run(build_number)}

      end
    end
  end
end