#!/usr/bin/ruby
lib_root = File.expand_path(File.dirname(__FILE__))
$: << File.join(lib_root, "fluentci")
$: << File.join(lib_root, "components")

Dir.glob(File.join(lib_root, 'fluentci/*.rb')).each {|f| require f }