module FluentCI
  module Components
    module JobStatus
      def self.new
        StoppedStatus.new
      end
      class Status
        @status = nil;
        def to_s
          @status
        end
        def success
          return self
        end

        def failure
          return self
        end

        def running
          return self
        end
      end
      class StoppedStatus < Status
        def initialize
          @status = "Stopped"
        end
        def running
          RunningStatus.new
        end
      end

      class RunningStatus < Status
        def initialize
          @status = "Running"
        end
        def success
          SuccessStatus.new
        end
        def failure
          FailureStatus.new
        end
      end

      class SuccessStatus < Status
        def initialize
          @status = "Success"
        end
      end

      class FailureStatus < Status
        def initialize
          @status = "Failure"
        end
      end
    end
  end
end