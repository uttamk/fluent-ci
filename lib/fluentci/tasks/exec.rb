module FluentCI
  module Tasks
    def self.exec cmd
      begin
        puts `#{cmd} 2>&1`
        if $?.exitstatus !=0
          raise "Command #{cmd} failed with exit status #{$?.exitstatus}"
        end
        return 0
      end
    end
  end
end