require 'fluentci/components/stage'

describe "Stage" do

  it "should add jobs and run them" do
    job1 = double("job1")
    job1.stub(:run)
    job2 = double("job2")
    job2.stub(:run)  
    

    stage = FluentCI::Components::Stage.new("test_stage")
    stage.add_job job1
    stage.add_job job2
    stage.run

    expect(stage.jobs.length).to eq(2)
    expect(stage.jobs[0]).to eq(job1)
    expect(stage.jobs[1]).to eq(job2)
  end

end
