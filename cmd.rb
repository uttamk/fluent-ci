#!/usr/bin/ruby
$: << '.'
require 'lib/fluentci'

if(ARGV.length <1)
  abort "Please supply the config file name to run"
end  

content = File.read("config/#{ARGV[0]}")
FluentCI::DSL.start
FluentCI::DSL.eval(content)
FluentCI::DSL.run