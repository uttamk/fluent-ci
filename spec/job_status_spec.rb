require 'fluentci/components/job_status'
describe "Job Status" do
  describe "Stopped Status" do
    it "should start with stopped status" do
      job_status = FluentCI::Components::JobStatus.new
      job_status.to_s.should eq "Stopped"
    end
    it "should transition to running" do
      job_status = FluentCI::Components::JobStatus.new
      job_status = job_status.running
      job_status.to_s.should eq "Running"
    end
  end

  describe "Running Status" do
    it "should stay in the running state when it receives a running message" do
      job_status = FluentCI::Components::JobStatus::RunningStatus.new
      job_status = job_status.running
      job_status.to_s.should eq "Running"
    end

    it "should move from running status to success on sucess" do
      job_status = FluentCI::Components::JobStatus::RunningStatus.new
      job_status = job_status.success
      job_status.to_s.should eq "Success"
    end

    it "should move from running status to failure on failure" do
      job_status = FluentCI::Components::JobStatus::RunningStatus.new
      job_status = job_status.failure
      job_status.to_s.should eq "Failure"
    end
  end

  describe "Sucess Status" do 
    it "should stay in success on success" do
      job_status = FluentCI::Components::JobStatus::SuccessStatus.new
      job_status = job_status.success
      job_status.to_s.should eq "Success"
    end

    it "should stay in success on failure" do
      job_status = FluentCI::Components::JobStatus::SuccessStatus.new
      job_status = job_status.failure
      job_status.to_s.should eq "Success"
    end 

    it "should stay in success on running" do
      job_status = FluentCI::Components::JobStatus::SuccessStatus.new
      job_status = job_status.running
      job_status.to_s.should eq "Success"
    end
  end

  describe "Failure Status" do 
    it "should stay in failure on success" do
      job_status = FluentCI::Components::JobStatus::FailureStatus.new
      job_status = job_status.success
      job_status.to_s.should eq "Failure"
    end

    it "should stay in failure on failure" do
      job_status = FluentCI::Components::JobStatus::FailureStatus.new
      job_status = job_status.failure
      job_status.to_s.should eq "Failure"
    end 

    it "should stay in failure on running" do
      job_status = FluentCI::Components::JobStatus::FailureStatus.new
      job_status = job_status.running
      job_status.to_s.should eq "Failure"
    end
  end
end
