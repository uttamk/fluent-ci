require 'FileUtils'
module FluentCI
  module Repositories
    class FileRepository
      def initialize base_dir
        @base_dir = base_dir
      end

      def add_build_for component_name
        add_component component_name

        current_build_file_path = "#{@base_dir}/#{component_name}/current_build_number"
        current_build_number = 0
        if(File.exists? current_build_file_path)
          File.open(current_build_file_path, "r") do |file|
            current_build_number = file.read().to_i
          end
        end
        current_build_number += 1
        File.write(current_build_file_path, current_build_number)
        return current_build_number
      end
      private
      def add_component component_name
        component_path = "#{@base_dir}/#{component_name}"
        FileUtils.mkdir_p(component_path) unless File.exists?(component_path)
      end
    end
  end
end