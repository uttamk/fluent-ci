require 'fluentci/tasks/exec'

describe "exec task" do
  it "should execute a valid command" do
    result = FluentCI::Tasks.exec("echo \"bar\"")

    result.should equal 0
  end

  it "should return throw exception on bad command" do
    begin
      result = FluentCI::Tasks.exec("some_randomcommand")
    rescue Exception => e
      e.should_not eq nil
    end

  end

  it "should return non zero when the shell returns non zero" do
    begin
      result = FluentCI::Tasks.exec("cat some_randomfile")
    rescue Exception => e
      e.should_not eq nil
      e.message.should include "Command cat some_randomfile failed with exit status"
    end
  end
end