
pipeline "My Code"

  stage "Compile and Unit Test"

    job "Compile" do 
      exec 'echo "Compiling"'
    end

    job "Unit Test" do
      puts "Unit testing"
    end

  stage "Javascript tests"

    job "Run jasmine" do
      puts "Running Jasmine tests"
    end

    job "Run Coverage reports" do
      puts "Running Coverage reports"
      exec "cat qwertyuiop.txt"
    end
