module FluentCI
  module Components
    class Stage
      
      attr_reader :jobs
      def initialize name
        @name = name
        @jobs = []
      end
    attr_reader :name

      def add_job job
        @jobs << job
      end

      def run build_number=0

        puts "Running Stage #{@name}"
        @jobs.each {|job|
          pid = fork {
            job.run build_number
          }
          Process.waitpid(pid, Process::WNOHANG)
        }

      end
    end
  end
end