require 'fluentci/components/job'
require 'fluentci/components/job'
require 'fluentci/dsl'

describe "Job" do

  it "should run job and redirect console output to jobname.log" do
    
    block = Proc.new {
      result = FluentCI::DSL::exec "echo \"testing!!!\""
      $stderr.puts "stderr"
    }
    job = FluentCI::Components::Job.new("test_job", block)
    job.run
    console_content = File.read("./0/test_job.log")
    expect(console_content).to eq("testing!!!\nstderr\n")
  end

  it "should run job and create the log directory as given in the config" do

    block = Proc.new {
      puts "testing!!!"
      $stderr.puts "stderr"
    }
    job = FluentCI::Components::Job.new("test_job", block, "test/foo")
    job.run
    console_content = File.read("./test/foo/0/test_job.log")
    expect(console_content).to eq("testing!!!\nstderr\n")
  end 

  it "should run job given a build number" do

    block = Proc.new {
      puts "testing!!!"
      $stderr.puts "stderr"
    }
    job = FluentCI::Components::Job.new("test_job", block, "test/foo")
    job.run 11
    console_content = File.read("./test/foo/11/test_job.log")
    expect(console_content).to eq("testing!!!\nstderr\n")
  end

  it "should write job success status to disk" do

    block = Proc.new {
      puts "testing!!!"
      $stderr.puts "stderr"
    }
    job = FluentCI::Components::Job.new("test_job", block, "test/foo")
    job.run 11
    console_content = File.read("./test/foo/11/test_job.log")
    expect(console_content).to eq("testing!!!\nstderr\n")
    status = File.read("./test/foo/11/test_job.status")
    status.should eq("Success")
  end

  it "should write job failure status to disk" do

    block = Proc.new {
     FluentCI::DSL::exec "cat sdfdsf"
    }
    job = FluentCI::Components::Job.new("test_job", block, "test/foo")
    job.run 11
    console_content = File.read("./test/foo/11/test_job.log")
    expect(console_content).to include "cat: sdfdsf: No such file or directory"
    status = File.read("./test/foo/11/test_job.status")
    status.should eq("Failure")
  end

  after :each do
    FileUtils.rm_rf("./0") unless !File.exists? "./0"
    FileUtils.rm_rf("./test") unless !File.exists? "./test"
  end

end