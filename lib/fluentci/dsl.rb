require 'engine'
require 'tasks/exec'
require 'repositories/file_repository'
module FluentCI
  module DSL
    @@engine = nil

    def self.start
     @@engine = FluentCI::Engine.new("./workspace",FluentCI::Repositories::FileRepository.new("./workspace"))
    end

    def self.engine
     @@engine
    end

    def self.eval content
     class_eval content
    end

    def self.pipeline name
      @@engine.add_pipeline name
    end

    def self.stage name
      @@engine.add_stage name
    end

    def self.job name, &block
      @@engine.add_job name, block
    end

    def self.run pipeline_name=nil
      @@engine.run pipeline_name
    end

    def self.exec cmd
      FluentCI::Tasks::exec cmd
    end
  end
end