require 'components/pipeline'
require 'components/stage'
require 'components/job'

module FluentCI

  class Engine

    def initialize workspace_location, build_manager
      @pipelines = []
      @current_pipeline = nil
      @current_stage = nil
      @current_build = 0
      @build_manager = build_manager
      @workspace_location = workspace_location
    end
    attr_reader :current_job
    attr_reader :pipelines

    def add_pipeline name
      new_pipeline = Components::Pipeline.new(name)
      @pipelines << new_pipeline
      @current_pipeline = new_pipeline
    end

    def add_stage stage_name
      new_stage = Components::Stage.new(stage_name)
      @current_pipeline.add_stage(new_stage)
      @current_stage = new_stage
    end

    def add_job job_name, block
      @current_job = Components::Job.new(job_name, block, "#{@workspace_location}/#{@current_pipeline.name}/#{@current_stage.name}")
      @current_stage.add_job(@current_job)
    end

    def run pipeline_name=nil

      if(pipeline_name == nil)
        pipeline_to_run = @current_pipeline 
      else
        pipeline_to_run = (@pipelines.select {|pipeline| pipeline.name == pipeline_name}).first()
      end
      current_build_number = @build_manager.add_build_for(pipeline_to_run.name)
      pipeline_to_run.run(current_build_number)
    end
  end
end