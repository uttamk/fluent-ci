require 'repositories/file_repository'
describe "File repository" do

  it "should save build number as 1 for the first time" do

    repository = FluentCI::Repositories::FileRepository.new "./test_workspace"
    repository.add_build_for("test_pipeline").should eq 1

    (File.read "./test_workspace/test_pipeline/current_build_number").should eq "1"
  end

  it "should increment build number every subsequent time" do

    repository = FluentCI::Repositories::FileRepository.new "./test_workspace"

    repository.add_build_for("test_pipeline").should eq 1
    (File.read "./test_workspace/test_pipeline/current_build_number").should eq "1"

    repository.add_build_for("test_pipeline").should eq 2
    (File.read "./test_workspace/test_pipeline/current_build_number").should eq "2"

    repository.add_build_for("test_pipeline").should eq 3
    (File.read "./test_workspace/test_pipeline/current_build_number").should eq "3"
  end

  after(:each) do
    `rm -rf ./test_workspace`
  end
end